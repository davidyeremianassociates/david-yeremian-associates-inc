David Yeremian & Associates, Inc. is an employment law firm based in Glendale, CA, serving employees across the state of California. We deal with everything from pay issues to discrimination and harassment, and we can help you get the protection and advice you need.

Address: 535 N Brand Blvd, #705, Glendale, CA 91203, USA

Phone: 818-230-8380

Website: https://www.yeremianlaw.com
